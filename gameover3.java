import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class gameover3 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class gameover3 extends Actor
{
    /**
     * Act - do whatever the gameover3 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
     
    public gameover3()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        GreenfootImage myImage = getImage();
        int myNewHeight = (int)myImage.getHeight()*4;
        int myNewWidth = (int)myImage.getWidth()*4;
        myImage.scale(myNewWidth, myNewHeight);
    }
}
