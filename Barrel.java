import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Barrel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Barrel extends Actor
{
     int timerBarrel = 500;  
    /**
     * Act - do whatever the Barrel wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        timerBarrel--;
        if (timerBarrel ==0)
        {
            getWorld().removeObject(this);
        }
    }   
    
    private boolean atRightEdge()
    {
        if(getX() > 10)
            return true;
        else
            return false;
    } 
     
    private boolean atBottomEdge()
    {
        if(getY() > 10)
            return true;
        else
            return false;
    }
    
    private boolean atTopEdge()
    {
        if(getY() > 10)
            return true;
        else
            return false;
    }
    
    private boolean atLeftEdge()
    {
        if(getX() > 10)
            return true;
        else
            return false;
    }
 
}

