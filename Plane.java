import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Pesawat1 here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Plane extends Actor
{
    /**
     * Act - do whatever the Pesawat1 wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public int belok;
    int jumlahRocket=0;
    int jumlahBarrel=0;
    public void act() 
    {
        // Add your action code here.
        handleMovement();
        grabBarrel();
        rocketCollision();
        getWorld().showText("Score: "+jumlahBarrel,40,10);
        getWorld().showText("Rocket: "+jumlahRocket,750,10);
        cekSkor();
    }    
    
    public void handleMovement()
    {
        move(2);
        if(Greenfoot.isKeyDown("left"))
        {
            belok-=2;
            setRotation(belok);
        }
        if(Greenfoot.isKeyDown("right"))
        {
            belok+=2;
            setRotation(belok);
        }
    }
    
    public void rocketCollision()
    {
        Actor getRocket = getOneIntersectingObject(Rocket.class);
        if(getRocket != null)
        {
            getWorld().removeObject(getRocket);
            setLocation(50,200);
            Greenfoot.playSound("Bang.wav");
            jumlahRocket +=1;
        }
    }
    
    public void grabBarrel()
    {
        if(isTouching(Barrel.class))
        {
            removeTouching(Barrel.class);
            getWorld().addObject (new Barrel(), (Greenfoot.getRandomNumber(800)), (Greenfoot.getRandomNumber(600)));
            Greenfoot.playSound("Syuuu1.wav");
            jumlahBarrel +=2;
        }
    }
    
    public void cekSkor(){
        if(jumlahRocket >5){
            gameover3 alert_kalah = new gameover3();
            Greenfoot.playSound("gameover.wav");
            getWorld().addObject(alert_kalah, 400, 300);
            Greenfoot.stop();
        }
        if(jumlahBarrel >50){
            win alert_win = new win();
            Greenfoot.playSound("winn.mp3");
            getWorld().addObject(alert_win, 400, 300);
            Greenfoot.stop();
        }
    }
    
}

